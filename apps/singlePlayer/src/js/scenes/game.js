/* 
Labyrinthklasse.
*/


import { Player } from '../actors/player';
import { loader } from '../utils/loaders';

export class Game extends Phaser.Scene {

  constructor(config) {
    super(config);
  }

  preload() {
    loader(this);
  }

  create() {

    //erzeugen der Tilemap
    const map = this.make.tilemap({ key: "map" });
    const tileset = map.addTilesetImage("blocks", "tiles");
    const worldLayer = map.createStaticLayer("ground", tileset, 0, 0);

    //Kollision über Arcadephyisk auf die Propertie der Tilemap festgelet
    worldLayer.setCollisionByProperty({ collide: true });

    // Maske für Taschenlampeneffekt
    // Alle Spieleelmente werden über Container maskiert so wird nur der "Schein" des Lichtkegels sichtbar
    var mask = this.make.image({
      x: 50,
      y: 50,
      key: 'mask',
      add: false,
    });
    const displaymask = new Phaser.Display.Masks.BitmapMask(this, mask);

    const worldContainer = this.add.container(0, 0);
    worldContainer.mask = displaymask;
    //Tilemap wird maskiert
    worldContainer.add(worldLayer);


    //Sammeln von initialen Positionsinformationen für das Item spawn und das teleporten
    //Ermitteln der möglichen Item Positionen
    this.itemPositions = this.genPositionArray('item', worldLayer);
    this.telePortPositions = this.genPositionArray('teleport', worldLayer);
    this.startPositions = this.genPositionArray('spawn', worldLayer);

    // Gruppe für Einsammelbare Objekte wird erstellt und für die Arcadephysik nutzbar gemacht und Initial Itemes erzeugt.
    this.collectables = this.physics.add.group();
    this.collectables.enableBody = true;
    this.addItem(this.itemPositions, this.collectables, worldContainer, 20);

    this.teleporter = this.physics.add.group();
    this.teleporter.enableBody = true;

    this.telePortPositions.forEach(pos => {
      var x = this.teleporter.create(pos.x, pos.y, this.add.sprite('teleporter'));
      x.play('teleportersphere');
      worldContainer.add(x);
    });


    //Spieler Figur konfiguiert und erzeugt.
    this.player = new Player({
      scene: this,
      key: 'mimi',
      pos: Phaser.Utils.Array.GetRandom(this.startPositions),
      acceleration: 60,
      walkingSound: this.sound.add('step'),
      light: mask,
      world: worldLayer,
      container: worldContainer,
      score: this.add.text(8, 8, 'Punkte: 0', { fontSize: '18px', fill: '#eee' }).setScrollFactor(0)
    });

    worldContainer.add(this.player);
    //Die Kamera auf den Spieler festgelegt. Der Bildausschnitt der Karte folgt dem Spiele.
    this.cameras.main.setBounds(0, 0, worldLayer.widthInPixels, worldLayer.heightInPixels);
    this.cameras.main.startFollow(this.player);

    this.openchest = this.sound.add('openChest');
    this.porting = this.sound.add('porting');
    this.physics.add.overlap(this.player, this.collectables, this.collect, null, this);
    this.physics.add.overlap(this.player, this.teleporter, this.beammeup, null, this);


    //Items Refilltimer - 
    this.time.addEvent({
      delay: 10000,
      repeat: -1,
      callbackScope: this,
      callback: function () {
        if (this.collectables.getLength() < 5) {
          this.addItem(this.itemPositions, this.collectables, worldContainer, 20);
        }
      }
    });
  }


  update(time, delta) {
   
  }

  //Einsammlen von Kisten
  collect(player, chest) {
    if (!chest.hit) {
      chest.hit = true;
      const scene = this;
      const tween = this.add.tween({
        targets: chest,
        scaleX: '+=.8',
        scaleY: '+=.8',
        alpha: { from: 1, to: 0 },
        duration: 1000,
        ease: 'Linear',
        repeat: 0,
        yoyo: false,
        onComplete: function () {
          scene.collectables.killAndHide(chest);
          scene.collectables.remove(chest);
          chest.enableBody = false;
        },
        onStart: function () {
          player.score = 10;
          scene.openchest.play();
        }
      });
    }
  }

  //Teleportieren zu einem zufälligen anderen Teleporter. Beamertimer
  beammeup(player, beamer) {
    if (!beamer.hit && !player.beamed) {
      beamer.hit = true;
      this.porting.play();
      const pos = Phaser.Utils.Array.GetRandom(this.telePortPositions);
      player.beamed = true;
      const scene = this;
      const rotation = player.rotation;
      const tween = this.add.tween({
        targets: player,
        scaleX: '+=.8',
        scaleY: '+=.8',
        rotation: 360,
        alpha: { from: 1, to: 0 },
        duration: 1000,
        ease: 'Linear',
        repeat: 0,
        yoyo: false,
        onComplete: function () {
          player.x = pos.x;
          player.y = pos.y;
          player.alpha = 1;
          player.scaleX = 1;
          player.scaleY = 1;
          player.rotation = rotation;
        },
        onStart: function () {
          scene.porting.play();
        }
      });
    }
  }

  //Hinzufügen von Kisten
  addItem(positions, group, container, amount) {
    for (var item = 0; item < amount; item++) {
      var pos = Phaser.Utils.Array.GetRandom(positions);
      var x = group.create(pos.x, pos.y, 'chest');
      container.add(x);
    }
  }

  //Ermitteln von Positionen von Keys auf der Tilemap
  genPositionArray(key, map) {
    var positions = []
    map.forEachTile(tiles => {
      if (tiles.properties[key]) {
        positions.push({ x: (tiles.x * tiles.height) + (tiles.height / 2), y: (tiles.y * tiles.height) + (tiles.height / 2) });
      }
    })
    return positions;
  }

}


