import { Bullet } from "./bullet";

export class Player extends Phaser.GameObjects.Sprite {


  constructor(config) {

    super(config.scene, config.pos.x, config.pos.y, config.key);

    config.scene.physics.world.enable(this);
    config.scene.physics.add.collider(this, config.world, function (o1, o2) { console.log("Wand!"); console.log(o1); console.log(o2) });
    config.scene.add.existing(this);

    this.acceleration = config.acceleration;
    this.light = config.light;
    this.create(config);
    this.scoreCount = 0;
    this.isBeamed = false;
    this.isFired = false;
    this.scoreText = config.score;
    this.config = config;
  }

  create(config) {
    this.keys = config.scene.input.keyboard.createCursorKeys()

    const footStepTimer = config.scene.time.addEvent({
      delay: config.walkingSound.duration * 1000,
      repeat: -1,
      callbackScope: this,
      callback: function () {
        if (this.isWalking) {
          config.walkingSound.play();
        }
      }
    });
    //Start - Figur schaut immer nach unten.
    this.direction = { x: 1, y: 0 };
  }


  preUpdate(time, delta) {
    super.preUpdate(time, delta);
    this.update(time, delta);
  }

  update(time, delta) {
    this.movement(this.keys);
  }


  movement(keys) {
    this.isWalking = true;
    if (keys.left.isDown && keys.up.isDown) {
      this.anims.play('walk_up', true);
      this.body.setVelocity(this.acceleration * -1, this.acceleration * -1);
      this.direction = { x: -1, y: -1 };
    } else if (keys.right.isDown && keys.up.isDown) {
      this.anims.play('walk_up', true);
      this.body.setVelocity(this.acceleration, this.acceleration * -1);
      this.direction = { x: 1, y: -1 };
    } else if (keys.left.isDown && keys.down.isDown) {
      this.anims.play('walk_down', true);
      this.body.setVelocity(this.acceleration * -1, this.acceleration);
      this.direction = { x: -1, y: 1 };
    } else if (keys.right.isDown && keys.down.isDown) {
      this.anims.play('walk_down', true);
      this.body.setVelocity(this.acceleration, this.acceleration);
      this.direction = { x: 1, y: 1 };
    } else if (keys.left.isDown) {
      this.anims.play('walk_left', true);
      this.body.setVelocity(this.acceleration * -1, 0);
      this.direction = { x: -1, y: 0 };
    } else if (keys.right.isDown) {
      this.anims.play('walk_right', true);
      this.body.setVelocity(this.acceleration, 0);
      this.direction = { x: 1, y: 0 };
    } else if (keys.up.isDown) {
      this.anims.play('walk_up', true);
      this.body.setVelocity(0, this.acceleration * -1);
      this.direction = { x: 0, y: -1 };
    } else if (keys.down.isDown) {
      this.anims.play('walk_down', true);
      this.body.setVelocity(0, this.acceleration);
      this.direction = { x: 0, y: 1 };
    } else {
      this.anims.stop();
      this.isWalking = false;
      this.body.setVelocity(0, 0);
    }

    this.light.x = this.x;
    this.light.y = this.y;

    if (keys.space.isDown) {
      this.fire(this.direction);
    }
  }


  fire(direction) {

    if (!this.isFired) {
      this.isFired=true;
      new Bullet({
        scene: this.config.scene,
        key: 'plasmaball',
        sound: this.config.scene.sound.add('bratzel'),
        parrent: this,
        direction: direction,
        world: this.config.world,
        container: this.config.container
      });

      this.config.scene.time.addEvent({
        delay: 800,
        repeat: 0,
        callbackScope: this,
        callback: function () {
          if (this.isFired) {
            this.isFired = false;
          }
        }
      });
    }
  }

  set score(count) {
    this.scoreCount += count;
    //Mehr Licht bei mehr Punkten : todo - weniger Punkte weniger licht - Trefferbehandlung für die Blitzkugel
    if (this.light.scaleY < 3) {
      this.light.scaleY += .2;
      this.light.scaleX += .2;
    }
    this.scoreText.text = "Punkte: " + this.scoreCount;
  }

  set beamed(beamed) {
    this.isBeamed = beamed;
    this.config.scene.time.addEvent({
      delay: 10000,
      repeat: 0,
      callbackScope: this,
      callback: function () {
        console.log("Beamtimer");
        if (this.beamed) {
          this.beamed = false;
          console.log("Beamtimer - reset");
        }
      }
    });
  }

  get beamed() {
    return this.isBeamed;
  }


}