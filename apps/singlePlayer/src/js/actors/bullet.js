export class Bullet extends Phaser.GameObjects.Sprite {

  /*
  Bullet wird an der Wand entfernt
  Noch keine weitere Treffer behandlung
  */
  constructor(config) {
    super(config.scene, config.parrent.x, config.parrent.y, config.key);
    config.scene.physics.world.enable(this);
    config.scene.physics.add.collider(this, config.world, function (o1, o2) {
      config.sound.stop();
      o1.destroy()
    });
    config.scene.add.existing(this);
    this.create(config);
    this.config = config;
  }

  create(config) {
    config.container.add(this);
    config.sound.loop = true;
    config.sound.play();
  }

  preUpdate(time, delta) {
    super.preUpdate(time, delta);
    this.update(time, delta);
  }

  update(time, delta) {
    this.anims.play('fire', true);
    this.body.setVelocity(this.config.direction.x * 120, this.config.direction.y * 120);
  }
}